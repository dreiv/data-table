import { records } from "./mocks";
import { sortByDirection, groupRecords } from "./helpers";

export async function loadRecords(
  page: number,
  pageSize: number,
  { withAllIds, sortBy, sortDirection, groupBy, filter, signal }: any = {}
): Promise<any> {
  if (signal?.aborted) {
    return Promise.reject("Aborted");
  }

  return new Promise((resolve, reject) => {
    const offset = (page - 1) * pageSize;
    const auxiliary = {} as any;
    let processedRecords = records;
    let groups;

    if (filter) {
      processedRecords = processedRecords.filter(({ type }) =>
        type.toLowerCase().includes(filter.toLowerCase())
      );
    }

    if (sortBy) {
      processedRecords.sort(sortByDirection(sortDirection, sortBy));
    }

    if (groupBy) {
      [groups, processedRecords] = groupRecords(processedRecords, groupBy);
      auxiliary.groups = groups;
    }

    if (withAllIds) {
      auxiliary.ids = processedRecords.map(({ id }) => id);
    }

    const data = {
      ...auxiliary,
      records: processedRecords.slice(offset, offset + pageSize),
      total: processedRecords.length,
    };

    const timeout = setTimeout(() => resolve(data), Math.random() * 500 + 300);
    if (signal) {
      signal.addEventListener("abort", () => {
        clearTimeout(timeout);

        reject("Aborted");
      });
    }
  });
}
