import { SortDirection } from "@/ui/components";
import { collator } from "./colator";

export const sortByDirection =
  (sortDirection: SortDirection, key: string) => (a: any, z: any) =>
    sortDirection === "down"
      ? collator.compare(a[key], z[key])
      : collator.compare(z[key], a[key]);
