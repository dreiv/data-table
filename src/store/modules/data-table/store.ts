import { shallowRef } from "vue";
import { defineStore } from "pinia";

import { loadRecords } from "@/services";

import { storedColumns } from "./storedColumns";
import { DataTableState } from "./types";
import { getRowsAndGroups } from "./helpers";
import { clone } from "@/helpers";
import { DatatableGroup } from "@/ui/components";

let controller: AbortController;

export const useDataTableStore = defineStore("dataTableStore", {
  state: (): DataTableState => ({
    columns: storedColumns,
    rows: [],
    rowsWithGroups: [],
    status: undefined,
    // dashboard
    filter: "",
    groupBy: "type",
    // pagination
    pageSize: 50,
    page: 1,
    totalPages: 0,
    // selection
    allRows: {},
    groups: {},
  }),
  actions: {
    async getDocuments(page: number, withAllIds = false) {
      if (controller) controller.abort();
      controller = new AbortController();
      this.status = "loading";

      try {
        const { records, total, ids, groups } = await loadRecords(
          page,
          this.pageSize,
          { groupBy: this.groupBy, withAllIds }
        );

        if (ids) {
          [this.allRows, this.groups] = getRowsAndGroups(groups, ids);
        }

        this.page = page;
        this.totalPages = Math.ceil(total / this.pageSize);
        this.rows = records.reduce((acc: any, row: any, idx: number) => {
          if (!idx || records[idx - 1][this.groupBy] !== row[this.groupBy]) {
            const groupName = row[this.groupBy];
            acc.push({
              component: shallowRef(DatatableGroup),
              id: row.id + groupName,
              count: this.groups[groupName].rows.length,
              groupName,
            });
          }
          acc.push(row);

          return acc;
        }, []);

        this.status = records.length ? "loaded" : "empty";
      } catch (error: any) {
        if (error === "Aborted") {
          this.status = "error";
        }
      }
    },
    filterBy(value: string) {
      this.filter = value;
      // todo refresh
    },

    selectRow(id: string, checked: boolean) {
      const row = this.allRows[id];
      const group = this.groups[row.group];

      row.selected = checked;
      group.selected = group.rows.every(
        (row) => this.allRows[row].selected
      );
    },

    selectGroup(targetGroup: string, checked: boolean) {
      const group = this.groups[targetGroup];
      const allRowsClone = clone(this.allRows);

      group.selected = checked;
      group.rows.forEach((id) => {
        this.allRows[id].selected = checked;
      });

      this.allRows = allRowsClone;
    },
  },
});
