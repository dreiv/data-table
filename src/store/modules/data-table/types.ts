import {
  DataTableColumn,
  DataTableRow,
  DatatableStatus,
  UserConfig,
} from "@/ui/components";

export interface DataTableState {
  columns: DataTableColumn[];
  rows: DataTableRow[];
  rowsWithGroups: DataTableRow[];
  status: DatatableStatus;
  // dashboard
  filter: string;
  groupBy: string;
  // pagination
  pageSize: number;
  page: number;
  totalPages: number;
  // selection
  allRows: RowsById;
  groups: Group;
}

export interface SavedConfig {
  key: string;
  config: UserConfig;
}

export interface RowsById {
  [key: string]: {
    group: string;
    selected: boolean;
  };
}

export interface Group {
  [key: string]: {
    rows: string[];
    selected: boolean;
  };
}
