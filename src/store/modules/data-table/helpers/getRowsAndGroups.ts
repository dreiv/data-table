import { Group, RowsById } from "../types";

export function getRowsAndGroups(groups: any, ids: any): [RowsById, Group] {
  const allRows: RowsById = {};
  const allGroups: Group = {};
  let index = 0;

  for (const { group, rowsTotal } of groups) {
    allGroups[group] = { rows: [], selected: true };
    for (let i = 0; i < rowsTotal; i++) {
      const rowIndex = ids[index++];
      allRows[rowIndex] = { group, selected: true };
      allGroups[group].rows.push(rowIndex);
    }
  }

  return [allRows, allGroups];
}
