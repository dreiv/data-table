import { InjectionKey } from "vue";

import { DataTableColumn, DataTableRow, DatatableStatus } from "./types";

export interface DataTableContext {
  columns: DataTableColumn[];
  rows: DataTableRow[];
  status?: DatatableStatus;
  gridTemplateColumns: any;
}

export const DataTableKey: InjectionKey<any> = Symbol(
  "DataTable state provider identifier"
);
