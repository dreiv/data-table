import { watchSyncEffect, shallowRef, readonly } from "vue";

export function eagerComputed(fn: any) {
  const result = shallowRef();
  watchSyncEffect(() => {
    result.value = fn();
  });

  return readonly(result);
}
